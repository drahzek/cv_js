var cv = {
    "general": {
        "name": "Adam Nowak",
        "email": "tesotyw@gmail.com",
        "phone": "555555555"
    },
    "education": [
        {
            "university": "Gdansk University of Technology",
            "degree": "Engineering Management, B.Eng.",
            "graduation": "2018"
        },
        {
            "university": "Gdansk University of Technology",
            "degree": "Power Engineering, B.Eng.",
            "graduation": "2017"
        }],
    "experience": [
        {
            "employer": "Gdansk University of Technology",
            "position": "Technician of Nothing",
            "period": "2018-2019"
        },
        {
            "employer": "Nowhere",
            "position": "Technician of the Abyss",
            "period": "2017-2018"
        }],
    "skills":  [
        "t1", "t2", "t3", "t4"
    ],
    "hobbies": [
        "t1", "t2", "t3", "t4"
    ]
};

function createCv(cvid) {
    var cvbox = document.getElementById(cvid);

    article = document.createElement("article");
    cvbox.appendChild(article);

    sectionInfo = createSection("general");
    article.appendChild(sectionInfo);

    var informationTable = createInformationTable(cv);
    sectionInfo.appendChild(informationTable);

    sectionEdu = createSection("education");
    article.appendChild(sectionEdu);

    var educationTable = createEducationTable(cv);
    sectionEdu.appendChild(educationTable);

    sectionExp = createSection("experience");
    article.appendChild(sectionExp);

    var experienceTable = createExperienceTable(cv);
    sectionExp.appendChild(experienceTable);

    sectionSkill = createSection("skill");
    article.appendChild(sectionSkill);

    var skillsValues = makeUL(cv.skill);
    sectionSkill.appendChild(skillsValues);

    sectionInter = createSection("hobbies");
    article.appendChild(sectionInter);

    footer = document.createElement("footer");
    article.appendChild(footer);



}

function makeUL(array) {
    var list = document.createElement('ul');
    for (var i = 0; i < array.length; i++) {
        var item = document.createElement('li');
        item.appendChild(document.createTextNode(array[i]));
        list.appendChild(item);
    }
    return list;
}


function createEducationTable(cv) {
    var table = document.createElement("table");
    var caption = document.createElement("caption");
    caption.innerText= "ukonczone studia";
    table.appendChild(caption);
    table.classList.add("tableBasic");

    table.appendChild(createSimpleRow("University", cv.education));
    table.appendChild(createSimpleRow("Degree", cv.education));
    table.appendChild(createSimpleRow("Graduation", cv.education));

    return table;
}

function createInformationTable(cv) {
    var table = document.createElement("table");
    var caption = document.createElement("caption");
    caption.innerText= "informacje";
    table.appendChild(caption);
    table.classList.add("tableBasic");
    table.appendChild(createSimpleRow("name", cv.general.name));
    table.appendChild(createSimpleRow("email", cv.general));
    table.appendChild(createSimpleRow("phone", cv.general));

    return table;
}

function createExperienceTable(cv) {
    var table = document.createElement("table");
    var caption = document.createElement("caption");
    caption.innerText = "doswiadczenie zawodowe";
    table.appendChild(caption);
    table.classList.add("tableBasic");
    table.appendChild(createSimpleRow("employer", cv.experience));
    table.appendChild(createSimpleRow("position", cv.experience));
    table.appendChild(createSimpleRow("period", cv.experience));
    return table;
}


function createHeader(h, text) {
    header = document.createElement("header");
    h = document.createElement(h);
    h.innerHTML = text;
    header.appendChild(h);
    return header;
}

function createSection(title) {
    var section = document.createElement("section");
    section.appendChild(createHeader("h3", title));
    return section;
}

function createSimpleRow(header, value) {
    var tr = document.createElement("tr");
    var th = document.createElement("th");
    th.innerHTML = header;
    tr.appendChild(th);
    var td = document.createElement("td");
    td.innerHTML = value;
    tr.appendChild(td);
    return tr;
}