// var cvinfo = {
//     "general": {
//         "name": "Adam Nowak",
//         "email": "tesotyw@gmail.com",
//         "phone": "555555555"
//     },
//     "education": [
//         {
//             "university": "Gdansk University of Technology",
//             "degree": "Engineering Management, B.Eng.",
//             "graduation": "2018"
//         },
//         {
//             "university": "Gdansk University of Technology",
//             "degree": "Power Engineering, B.Eng.",
//             "graduation": "2017"
//         }],
//     "experience": [
//         {
//             "employer": "Gdansk University of Technology",
//             "position": "Technician of Nothing",
//             "period": "2018-2019"
//         },
//         {
//             "employer": "Nowhere",
//             "position": "Technician of the Abyss",
//             "period": "2017-2018"
//         }],
//     "skills":  [
//         "t1", "t2", "t3", "t4"
//     ],
//     "hobbies": [
//         "t1", "t2", "t3", "t4"
//     ]
// };
//
// function fillOutCV(container) {
//
//     var cvcontainer = document.getElementById(container);
//     cv = cvinfo;
//     article = document.createElement("article");
//     cvcontainer.appendChild(article);
//
//     generalSection = createSection("General informations");
//     article.appendChild(generalSection);
//     var generalDetails = createGeneralDetails(cv);
//     generalSection.appendChild(generalDetails);
//
//     educationSection = createSection("Education");
//     article.appendChild(educationSection);
//     var educationDetails = createEducationDetails(cv);
//     educationSection.appendChild(educationDetails);
//
//     experienceSection = createSection("Experience");
//     article.appendChild(experienceSection);
//     var experienceDetails = createExperienceDetails(cv);
//     experienceSection.appendChild(experienceDetails);
// }
//
// function createGeneralDetails(cv) {
//     var table = document.createElement("table");
//
//     table.appendChild(createSimpleRow("name", cv.general.name));
//     table.appendChild(createSimpleRow("email", cv.general.email));
//     table.appendChild(createSimpleRow("phone", cv.general.phone));
//
//     return table;
// }
//
// function createEducationDetails(cv) {
//     var table = document.createElement("table");
//     for (var index in cv.experience) {
//         var intable = document.createElement("intable");
//         intable.appendChild(createSimpleRow("university", cv.education[index].university));
//         intable.appendChild(createSimpleRow("degree", cv.education[index].degree));
//         intable.appendChild(createSimpleRow("graduation date", cv.education[index].graduation));
//         table.appendChild(intable);
//         //
//         // table.appendChild(createSimpleRow("university", cv.education[index].university));
//         // table.appendChild(createSimpleRow("degree", cv.education[index].degree));
//         // table.appendChild(createSimpleRow("graduation date", cv.education[index].graduation));
//
//     }
//
//     return table;
// }
//
// function createExperienceDetails(cv) {
//     var table = document.createElement("table");
//     for (var index in cv.experience) {
//         var intable = document.createElement("intable");
//         intable.appendChild(createSimpleRow("employer", cv.experience[index].employer));
//         intable.appendChild(createSimpleRow("position", cv.experience[index].position));
//         intable.appendChild(createSimpleRow("period", cv.experience[index].period));
//         table.appendChild(intable);
//     }
//
//     return table;
// }
//
// function createSimpleRow(header, value) {
//     var tr = document.createElement("tr");
//     var th = document.createElement("th");
//     th.innerHTML = header;
//     tr.appendChild(th);
//     var td = document.createElement("td");
//     td.innerHTML = value;
//     tr.appendChild(td);
//     return tr;
// }
//
// function createSection(title) {
//     var section = document.createElement("section");
//     section.appendChild(createHeader("h4", title));
//     return section;
// }
//
// function createHeader(h, text) {
//     header = document.createElement("header");
//     h = document.createElement(h);
//     h.innerHTML = text;
//     header.appendChild(h);
//     return header;
// }
//
// function makeUL(array) {
//     var list = document.createElement('ul');
//     for (var i = 0; i < array.length; i++) {
//         var item = document.createElement('li');
//         item.appendChild(document.createTextNode(array[i]));
//         list.appendChild(item);
//     }
//     return list;
// }
//
